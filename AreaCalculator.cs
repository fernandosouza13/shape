public class AreaCalculator : Shape
{
    public double total { get; set; }
    public double TotalArea(Shape[] shapes)
    {
        //double total = 0;
        foreach (var shape in shapes)
        {
            total += shape.Area();
        }
        return total;
    }
}
