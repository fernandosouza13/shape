public class Circle : Shape
{
    public double Radius { get; set; }
    public string Formato { get; set; }

    public override double Area()
    {
        return Math.PI * Math.Pow(Radius, 2);
    }

        public override string Forma()
    {
        return Formato;
    }
}