public class Rectangle : Shape
{
    public double Width { get; set; }
    public double Height { get; set; }
    public string Formato { get; set; }

    public override double Area()
    {
        return Width * Height;
    }

    public override string Forma()
    {
        return Formato;
    }
}
