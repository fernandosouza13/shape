﻿void PrintArea(Shape shape)
{
    Console.WriteLine($"Area do {shape.Forma()}: {shape.Area()}");
}


Rectangle rectangle = new Rectangle { Width = 5, Height = 4, Formato = "Retangulo" };
Square square = new Square { SideLength = 5, Formato =  "Quadrado" };
Circle circle = new Circle { Radius = 5 , Formato = "Círculo" };
AreaCalculator areacalculator = new AreaCalculator { total = 0};

//PrintArea(formato);
PrintArea(rectangle); // Output: Area: 20
PrintArea(square);   // Output: Area: 25
PrintArea(circle);   // Output: Area: 12.5663706143592

