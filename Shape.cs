public class Shape
{
    public string Formato { get; set; }
    public virtual double Area()
    {
        return 0;
    }
    public virtual string Forma()
    {
        return Formato;
    }
    
}
