public class Square : Shape
{
    public double SideLength { get; set; }
    public string Formato { get; set; }

    public override double Area()
    {
        return SideLength * SideLength;
    }

        public override string Forma()
    {
        return Formato;
    }
}
